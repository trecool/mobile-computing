﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text.RegularExpressions;
using UnityEngine.SceneManagement;

public class TutorialController : MonoBehaviour
{

    private string geste;
    private string gesteAlt;
    private string gestenCheck;

    private int time;
    private int timeAlt;
    private int timeDiff;
    private int counter = 1;
   
    private mqttTest client;

    private GameObject canvas;
       
    private Renderer swipe1;
    private Renderer swipe2;
    private Renderer swipe3;

    private Renderer circle1;
    private Renderer circle2;
    private Renderer circle3;

    private Renderer key1;
    private Renderer key2;
    private Renderer key3;

    private Renderer screen1;
    private Renderer screen2;
    private Renderer screen3;

    public Material rot;

    private bool isRunning;
    private bool swipe = true;
    private bool circle = false;
    private bool screenTap = false;
    private bool keyTap = false;
    private bool finish = false;


    // Use this for initialization
    void Start()
    {

        // Zuordnung von externen Scripts und Komponenten anderer Objekte      
        client = GameObject.Find("MQTT").GetComponent<mqttTest>();

        canvas = GameObject.Find("UI-Abfrage");

        swipe1 = GameObject.Find("Swipe 1").GetComponent<Renderer>();
        swipe2 = GameObject.Find("Swipe 2").GetComponent<Renderer>();
        swipe3 = GameObject.Find("Swipe 3").GetComponent<Renderer>();

        circle1 = GameObject.Find("Circle 1").GetComponent<Renderer>();
        circle2 = GameObject.Find("Circle 2").GetComponent<Renderer>();
        circle3 = GameObject.Find("Circle 3").GetComponent<Renderer>();

        screen1 = GameObject.Find("Screen 1").GetComponent<Renderer>();
        screen2 = GameObject.Find("Screen 2").GetComponent<Renderer>();
        screen3 = GameObject.Find("Screen 3").GetComponent<Renderer>();

        key1 = GameObject.Find("Key 1").GetComponent<Renderer>();
        key2 = GameObject.Find("Key 2").GetComponent<Renderer>();
        key3 = GameObject.Find("Key 3").GetComponent<Renderer>();

        canvas.SetActive(false);

        // Starten der Coroutine
        StartCoroutine(ModeSelect());

    }

    // Update is called once per frame
    void Update()
    {
        // Falls Coroutine beendet wurde, dann wieder starten  
        if (isRunning == false)
        {
            StartCoroutine(ModeSelect());
        }

    }

    // MQTT Nachrichten werden lokal gespeichert und verarbeitet 
    void messagePreFilter()
    {
        geste = client.nachricht;

        if (geste != null)
        {
            // Trennung von Nachricht und Timestamp
            string[] splitTime = Regex.Split(geste, "\\.");
            if (splitTime.Length > 1)
            {
                time = int.Parse(splitTime[1]);
            }
        }
                // Timestamps der letzten zwei Nachrichten werden verrechnet      
        timeDiff = time - timeAlt;

        // Absicherung vor doppeltem Versenden gleicher Gesten, im Anschluss Aufruf des MainFilters
        if (gesteAlt != geste)
        {
            messageMainFilter(geste);
        }
        timeAlt = time;
        gesteAlt = geste;
        geste = null;
        time = 0;
    }

    // Verarbeitung der nun relevanten Gesten
    void messageMainFilter(string geste)
    {
        if(geste.Contains ("swipe") && swipe == true)
        {
            switch (counter)
            {
                case 1:
                    swipe1.material.color = Color.green;
                    counter++;
                    break;
                case 2:
                    swipe2.material.color = Color.green;
                    counter++;
                    break;
                case 3:
                    swipe3.material.color = Color.green;
                    counter = 1;
                    swipe = false;
                    circle = true;
                    circle1.material = rot;
                    circle2.material = rot;
                    circle3.material = rot;
                    break;                               
            }
        }

        if (geste.Contains("circle") && circle == true)
        {
            switch (counter)
            {
                case 1:
                    circle1.material.color = Color.green;
                    counter++;
                    break;
                case 2:
                    circle2.material.color = Color.green;
                    counter++;
                    break;
                case 3:
                    circle3.material.color = Color.green;
                    counter = 1;
                    circle = false;
                    screenTap = true;
                    screen1.material = rot;
                    screen2.material = rot;
                    screen3.material = rot;
                    break;
            }
        }

        if (geste.Contains("screenTap") && screenTap == true)
        {
            switch (counter)
            {
                case 1:
                    screen1.material.color = Color.green;
                    counter++;
                    break;
                case 2:
                    screen2.material.color = Color.green;
                    counter++;
                    break;
                case 3:
                    screen3.material.color = Color.green;
                    counter = 1;
                    screenTap = false;
                    keyTap = true;
                    key1.material = rot;
                    key2.material = rot;
                    key3.material = rot;
                    break;
            }

        }

        if (geste.Contains("keyTap") && keyTap == true)
        {
            switch (counter)
            {
                case 1:
                    key1.material.color = Color.green;
                    counter++;
                    break;
                case 2:
                    key2.material.color = Color.green;
                    counter++;
                    break;
                case 3:
                    key3.material.color = Color.green;
                    keyTap = false;
                    finish = true;
                    canvas.SetActive(true);               
                    break;
            }

            if (geste.Contains("circle") && finish == true)
            {

                SceneManager.LoadScene(1);
            }

        }

    }
           
    IEnumerator ModeSelect()
    {
        isRunning = true;       
        messagePreFilter();
        yield return new WaitForSeconds(1);
        isRunning = false;
    }
    
}
