﻿using UnityEngine;
using System.Collections;

/* Der TexturePlayer basiert auf einem Ansatz von http://bernieroehl.com/360stereoinunity/ */

public class TexturePlayer : MonoBehaviour
{
    public GameObject sphere;
    public Renderer rend;
    public MeshRenderer mesh;

    public int numberOfFrames = 1018;
    public int currentFrame;

    public float frameRate = 30;
    public float seconds = 0f;

    public bool isRunning;
    public bool counting;
    public bool mode = false;

    private Texture2D[] frames;

    void Start()
    {
        //Renderer des Objektes holen um später Textur festlegen zu können
        rend = sphere.GetComponent<Renderer>();
        mesh = sphere.GetComponent<MeshRenderer>();

        isRunning = false;

        // Frames in Array laden
        frames = new Texture2D[numberOfFrames];
        for (int i = 0; i < numberOfFrames; ++i)
        {
            // Einlesen der Frames aus Ressourcenordner
            frames[i] = (Texture2D)Resources.Load(string.Format("frame{0:d4}", i + 1));
        }
    }

    void Update()
    {
        if (isRunning == true)
        {
            currentFrame = (int)(seconds * frameRate);

            // falls am Ende der Frames angekommen, dann loopen
            if (currentFrame >= frames.Length)
            {
                timerStatus("stop");
                seconds = 14;
                currentFrame = 420;
                timerStatus("start");
            }

            // aktueller Frame wird als Textur auf die Sphere gelegt
            rend.material.mainTexture = frames[currentFrame];
        }
    }

    // Steuerung für Timer, der alle 0.09 Sekunden customTimer aufrust (Werte basieren auf Tests)
    public void timerStatus(string status)
    {
        if (status == "start")
        {
            InvokeRepeating("customTimer", 1, 0.09f);
        }
        if (status == "stop")
        {
            CancelInvoke();
        }
    }

    // Timer, der unabhängig von Time.time Sekunden zählt
    void customTimer()
    {
        counting = true;
        seconds = seconds + 0.1f;
    }

}