﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;


public class getValues : MonoBehaviour {

    private mqttTest client;
    private string koordinatenRaw;
    private float x;
    private float y;
    private float z;
    private Vector3 tracking;


    // Use this for initialization
    void Start () {

        client = GameObject.Find("MQTT").GetComponent<mqttTest>();
        
        
    }
	
	// Update is called once per frame
	void Update () {

        retrieveCoords();
        transform.localPosition = tracking;
                        
    }


    void retrieveCoords()
    {
        koordinatenRaw = client.nachricht;
        string[] splitData = Regex.Split(koordinatenRaw, ",");
        x = float.Parse(splitData[0]) /10 *-1;
        y = float.Parse(splitData[2]) /10 *-1;
        z = float.Parse(splitData[1]) /10;
        tracking = new Vector3(x, y, z);
        Debug.Log(tracking);

    }
}
