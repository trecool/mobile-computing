﻿using UnityEngine;
using System.Collections;

public class GestenControls : MonoBehaviour {

    private string geste;
    private Renderer rend;
    private mqttTest client;
    private string swipe = "swipe";
    private string screenTap = "screenTap";

    // Use this for initialization
    void Start () {

        rend = GetComponent<Renderer>();
        client = GameObject.Find("MQTT").GetComponent<mqttTest>();
	}
	
	// Update is called once per frame
	void Update () {

        //geste = client.Gesten;
        //Debug.Log(geste);

        if (geste.Contains (swipe)) {
            rend.material.color = Color.red;

         }

        if (geste.Contains(screenTap)) {
            rend.material.color = Color.green;
        }
    }
}
