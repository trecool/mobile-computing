﻿using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System.Collections;


public class JSONSerializer : MonoBehaviour {

    public string jsonFile;
    public string test;    
    
    public JSONFrage json;
    private TexturePlayer player;

    public Text head;
    public Text body;
   
    private int roundedSeconds;

    private bool textStarted = false;

    private MeshRenderer videoMesh;
    
    
    

     
    void Start()
    {
        // Zuordnung von externen Scripts und Komponenten anderer Objekte  
        videoMesh = GameObject.Find("Videosphere").GetComponent<MeshRenderer>();
        player = GameObject.Find("VPO").GetComponent<TexturePlayer>();
        head = GameObject.Find("Head").GetComponent<Text>();
        body = GameObject.Find("Body").GetComponent<Text>();
               
        // Der String jsonFile wird mit dem Inhalt der angegebenen JSON-Datei befüllt
        jsonFile = File.ReadAllText(Application.dataPath + "/JSON/lesson.json");    
        // Ein Objekt json wird aus der jsonFile erstellt    
        json = CreateFromJSON(jsonFile);
      
    }

    // Update is called once per frame
    void Update ()
    {
        // Float Sekunden aus dem TexturePlayer werden in int umgewandelt, sodass es eine genaue Zuordnung geben kann
        roundedSeconds = (int)player.seconds;

        // Falls Sekunde X im Video mit der startTime eines UI-Elements übereinstimmt, so wird die Coroutine textDuration() aufgerufen
        if (roundedSeconds == json.startTime && textStarted == false && player.mode == true)
        {
            textStarted = true;
            Debug.Log("Text");
            StartCoroutine(textDuration(json.duration));
        }
    }

    // Basierend auf der Klasse JSONFrage wird ein Objekt mit den Eigenschaften der Klasse erstellt und mit den Inhalten der JSON-Datei befüllt
    public static JSONFrage CreateFromJSON(string jsonFile)
    {
        return JsonUtility.FromJson<JSONFrage>(jsonFile);
    }

    // Steuert die Dauer der Anzeige der Frage aus der JSON-Datei
    IEnumerator textDuration(float duration)
    {
        // Aufgrund von Culling-Fehlern des invertierten Meshes der Videosphere, muss das Mesh zur Anzeige von Fragen deaktiviert werden
        videoMesh.enabled = false;
        player.timerStatus("stop");

        // Die Fragen im UI werden mit den Inhalten der JSON-Datei vefüllt
        head.text = json.head;
        body.text = json.body;

        // Die Fragen oder Anmerkungen werden für den festgelegten Zeitraum dargestellt
        yield return new WaitForSeconds(duration);

        // Das Mesh wird wieder aktiviert und die Wiedergabe fortgesetzt
        videoMesh.enabled = true;
        player.timerStatus("start");
        body.text = null;
        head.text = null;
    }


}

// Serialisierbare Klasse, auf Basis welcher Objekte aus JSON-Dateien erstellt werden
[System.Serializable]
public class JSONFrage : System.Object
{
    public float x;
    public float y;
    public float z;
    public float startTime;
    public float duration; 

    public string type;
    public string topic;
    public string head;
    public string body;    

}
