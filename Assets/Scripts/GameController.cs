﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text.RegularExpressions;

public class GameController : MonoBehaviour
{
    private string geste, gesteAlt, gestenCheck;

    private int time, timeAlt, timeDiff;
    private int counter = 1;

    private mqttTest client;

    private Rotation grün, blau, rot, zylinder;

    private Renderer rendZylinder, rendRot, rendGrün, rendBlau;

    private Color rotColor, grünColor, blauColor;

    private TexturePlayer player;

    private bool isRunning = false;
    private bool menuControls = true;
    private bool videoControls = false;
    private bool sceneChooser = false;
    private bool acceptChange = false;
    private bool mode = false;

    private Text UIText;

    private GameObject camTransform;

    // Use this for initialization
    void Start()
    {

        // Zuordnung von externen Scripts und Komponenten anderer Objekte      
        client = GameObject.Find("MQTT").GetComponent<mqttTest>();
        player = GameObject.Find("VPO").GetComponent<TexturePlayer>();
        rot = GameObject.Find("Rot").GetComponent<Rotation>();
        grün = GameObject.Find("Grün").GetComponent<Rotation>();
        blau = GameObject.Find("Blau").GetComponent<Rotation>();
        zylinder = GameObject.Find("ModeChooser").GetComponent<Rotation>();
        rendZylinder = GameObject.Find("ModeChooser").GetComponent<Renderer>();
        rendRot = GameObject.Find("Rot").GetComponent<Renderer>();
        rendGrün = GameObject.Find("Grün").GetComponent<Renderer>();
        rendBlau = GameObject.Find("Blau").GetComponent<Renderer>();

        rotColor = rendRot.material.GetColor("_Color");
        grünColor = rendGrün.material.GetColor("_Color");
        blauColor = rendBlau.material.GetColor("_Color");

        UIText = GameObject.Find("Abfragetext").GetComponent<Text>();
        camTransform = GameObject.Find("CardboardMain");

        // Starten der Coroutine
        StartCoroutine(ModeSelect());

    }

    // Update is called once per frame
    void Update()
    {
        // Falls Coroutine beendet wurde, dann wieder starten  
        if (isRunning == false)
        {
            StartCoroutine(ModeSelect());
        }

    }

    // MQTT Nachrichten werden lokal gespeichert und verarbeitet 
    void messagePreFilter(string modus)
    {
        geste = client.nachricht;

        if (geste != null)
        {
            // Trennung von Nachricht und Timestamp
            string[] splitTime = Regex.Split(geste, "\\.");
            if (splitTime.Length > 1)
            {
                time = int.Parse(splitTime[1]);
            }
        }

        // Timestamps der letzten zwei Nachrichten werden verrechnet      
        timeDiff = time - timeAlt;

        // Absicherung vor doppeltem Versenden gleicher Gesten, im Anschluss Aufruf des MainFilters
        if (gesteAlt != geste)
        {
            messageMainFilter(geste, modus);
        }
        timeAlt = time;
        gesteAlt = geste;
        geste = null;
        time = 0;
    }

    // Verarbeitung der nun relevanten Gesten
    void messageMainFilter(string geste, string modus)
    {
        // Modus Menü: Verarbeitung von Swipe-Gesten zur Szenenwahl und Starten von Szenen
        if (modus == "menu")
        {
            // Bewegung der Kugeln nach rechts               
            if (geste.Contains("swipeRight"))
            {
                rot.rotation(120);
                grün.rotation(120);
                blau.rotation(120);

                if (counter < 3)
                {
                    counter++;
                }
                else {
                    counter = 1;
                }

                Debug.Log(counter);
            }

            //Bewegung der Kugeln nach links
            if (geste.Contains("swipeLeft"))
            {
                rot.rotation(-120);
                grün.rotation(-120);
                blau.rotation(-120);

                if (counter < 3)
                {
                    counter--;
                }
                else {
                    counter = 1;
                }

                Debug.Log(counter);
            }

            // Aufruf der Coroutine Abfrage() zur Bestätigung der Eingabe
            if (geste.Contains("keyTap"))
            {
                StartCoroutine(Abfrage());
            }
        }

        if (modus == "chooser")
        {
            // Bewegung der Kugeln nach rechts               
            if (geste.Contains("swipeRight"))
            {
                zylinder.rotationZylinder(-1);
                mode = !mode;
            }

            //Bewegung der Kugeln nach links
            if (geste.Contains("swipeLeft"))
            {
                zylinder.rotationZylinder(1);
                mode = !mode;
            }

            // Aufruf der Coroutine Abfrage() zur Bestätigung der Eingabe
            if (geste.Contains("keyTap"))
            {
                acceptChange = true;
                StartCoroutine(Abfrage());
            }
        }

        // Modus Video: Play / Pause vom Video sowie Rückkehr ins Menü
        if (modus == "video")
        {
            if (geste.Contains("circle"))
            {
                camTransform.transform.position = new Vector3(0, 0.5f, -2);

                menuController();
                player.mesh.enabled = false;
                player.currentFrame = 0;
                player.seconds = 0;

                rendZylinder.material.color = Color.gray;
                rendRot.material.color = rotColor;
                rendGrün.material.color = grünColor;
                rendBlau.material.color = blauColor;

                videoControls = false;
                menuControls = true;
                acceptChange = false;
            }

            // Pausieren und Starten
            if (geste.Contains("screenTap"))
            {
                menuController();
            }

            // Fünf Sekunden zurück skippen
            if (geste.Contains("swipeRight"))
            {
                player.seconds = player.seconds - 5f;
                player.currentFrame = player.currentFrame - 150;
            }
            // Fünf Sekunden vor skippen
            if (geste.Contains("swipeLeft"))
            {
                player.seconds = player.seconds + 5f;
                player.currentFrame = player.currentFrame + 150;
            }
        }
    }

    // Steuerung des TexturePlayers
    void menuController()
    {
        if (!player.isRunning)
        {            
            player.mesh.enabled = true;
            
            player.mode = mode;
            player.isRunning = true;
            player.timerStatus("start");           
        }

        else 
        {
            player.isRunning = false;
            player.timerStatus("stop");            
        }
    }

    // Ruft je nach aktuellem Modus den PreFilter der Nachrichten auf, um diese entsprechend für den MainFilter vorzubereiten 
    IEnumerator ModeSelect()
    {
        isRunning = true;
        if (menuControls == true && videoControls == false && sceneChooser == false)
        {
            messagePreFilter("menu");
        }

        if (menuControls == true && videoControls == false && sceneChooser == true)
        {
            messagePreFilter("chooser");
            Debug.Log("Abfrage");
        }
        if (videoControls == true && menuControls == false)
        {
            messagePreFilter("video");
        }


        // Wartet immer eine Sekunde, bevor eine neue Eingabe akzeptiert wird. Dies soll das Erkennen von verbuggten Gesten verhindern
        yield
        return new WaitForSeconds(1);
        isRunning = false;
    }

    // Überprüft ob eine Szenenauswahl wirklich vorgenommen werden soll indem eine Bestätigung gefordert wird
    IEnumerator Abfrage()
    {
        UIText.text = "Bestätigung durch erneuten Tap";
        Debug.Log("Starte Abfrage");
        yield
        return new WaitForSeconds(1.5f);

        /* Nach 1.5 Sekunden wird die letzte gespeicherte Geste mit der neuen verglichen. 
          Sind die gleich, so hat der User keine Bestätigung ausgelöst und er verbleibt im Menü,
          sollte die neue Geste allerdings neu sein und mit ausreichender Zeit zwischen den Gesten aufgerufen worden sein, so wird die Szene geöffnet
        */

        if (client.nachricht != geste && client.nachricht.Contains("keyTap") && timeDiff >= 500)
        {
            if (sceneChooser == false)
            {
                rendZylinder.material.color = Color.green;
                rendRot.material.color = Color.gray;
                rendGrün.material.color = Color.gray;
                rendBlau.material.color = Color.gray;
                chooseMode();
            }

            if (sceneChooser == true && acceptChange == true)
            {
                sceneChooser = false;

                startScene();
            }

        }

        UIText.text = null;
        Debug.Log("Abfrage Ende");
    }

    void startScene()
    {

        videoControls = true;
        menuControls = false;

        // basierend auf dem Counter (der aktuelle gewählten Szene) wird die Kamera in die entsprechende Szene verschoben
        if (counter == 1)
        {
            camTransform.transform.position = new Vector3(0, 3, -0.5f);            
        }

        if (counter == 2)
        {
            camTransform.transform.position = new Vector3(3, -2, 0);
        }

        if (counter == 3)
        {
            camTransform.transform.position = new Vector3(6, -2, 0);            
        }

    }

    void chooseMode()
    {
        sceneChooser = true;
        messagePreFilter("chooser");
        return;
    }
}