﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

 	}

    // Rotiert die Objekte um den Punkt 0,1,0 um degrees (120° oder -120°)
    public void rotation(int degrees)
    {
        transform.RotateAround(new Vector3(0, 0, 1), Vector3.up, degrees);
    }

    public void rotationZylinder(int direction)
    {
        transform.Rotate(0, 180 * direction, 0);
        Debug.Log("Funktion");
    }

  
}
