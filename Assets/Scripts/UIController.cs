﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text.RegularExpressions;

public class UIController : MonoBehaviour
{

    private string geste, gesteAlt, gestenCheck;

    private int time, timeAlt, timeDiff;
    private int counter;

    private mqttTest client;

    private Text frage1, frage2, frage3;

    private Image antwort1, antwort2, antwort3;

    private int answer;

    private bool isRunning;
    public bool answered;


    // Use this for initialization
    void Start()
    {
        counter = 1;
        
        // Zuordnung von externen Scripts und Komponenten anderer Objekte      
        client = GameObject.Find("MQTT").GetComponent<mqttTest>();

        frage1 = GameObject.Find("Body 1").GetComponent<Text>();
        frage2 = GameObject.Find("Body 2").GetComponent<Text>();
        frage3 = GameObject.Find("Body 3").GetComponent<Text>();

        antwort1 = GameObject.Find("Antwort 1").GetComponent<Image>();
        antwort2 = GameObject.Find("Antwort 2").GetComponent<Image>();
        antwort3 = GameObject.Find("Antwort 3").GetComponent<Image>();

        antwort1.color = Color.cyan;

        /*
        Hier würde die entsprechenden Parameter aus der JSON-Datei ausgelesen werden
        answer = json.antwort;

        frage1 = json.frage1;
        frage2 = json.frage2;
        frage3 = json.frage3;

        antwort1 = json.antwort1;
        antwort2 = json.antwort2;
        antwort3 = json.antwort3;
        */

        // Starten der Coroutine
        StartCoroutine(ModeSelect());

    }

    // Update is called once per frame
    void Update()
    {
        // Falls Coroutine beendet wurde, dann wieder starten  
        if (!isRunning)
        {
            StartCoroutine(ModeSelect());
        }
    }

    // MQTT Nachrichten werden lokal gespeichert und verarbeitet 
    void messagePreFilter()
    {
        geste = client.nachricht;

        if (geste != null)
        {
            // Trennung von Nachricht und Timestamp
            string[] splitTime = Regex.Split(geste, "\\.");
            if (splitTime.Length > 1)
            {
                time = int.Parse(splitTime[1]);
            }
        }
        // Timestamps der letzten zwei Nachrichten werden verrechnet      
        timeDiff = time - timeAlt;

        // Absicherung vor doppeltem Versenden gleicher Gesten, im Anschluss Aufruf des MainFilters
        if (gesteAlt != geste)
        {
            messageMainFilter(geste);
        }
        timeAlt = time;
        gesteAlt = geste;
        geste = null;
        time = 0;
    }

    // Verarbeitung der nun relevanten Gesten
    void messageMainFilter(string geste)
    {
        if (geste.Contains("Left"))
        {
            switch (counter)
            {
                case 1:
                    antwort1.color = Color.cyan;
                    antwort2.color = Color.grey;
                    antwort3.color = Color.grey;
                    counter = 1;
                    break;
                case 2:
                    antwort1.color = Color.cyan;
                    antwort2.color = Color.grey;
                    antwort3.color = Color.gray;
                    counter--;
                    break;
                case 3:
                    antwort1.color = Color.grey;
                    antwort2.color = Color.cyan;
                    antwort3.color = Color.grey;
                    counter--;
                    break;
            }
        }
        else if (geste.Contains("Right"))
        {
            switch (counter)
            {
                case 1:
                    antwort1.color = Color.grey;
                    antwort2.color = Color.cyan;
                    antwort3.color = Color.gray;
                    counter++;
                    break;
                case 2:
                    antwort1.color = Color.grey;
                    antwort2.color = Color.grey;
                    antwort3.color = Color.cyan;
                    counter++;
                    break;
                case 3:
                    antwort1.color = Color.grey;
                    antwort2.color = Color.grey;
                    antwort3.color = Color.cyan;
                    counter = 3;
                    break;
            }
        }

        /* Funktion zur Überprüfung der Antwort. Stimmt die Auswahl mit der Antwort überein, so wird sie grün unterlegt. Falls nicht, dann rot. 
        if (geste.Contains("Down"))
         {
             if(counter == answer)
             {
                 antwort.counter = Color.green;
                 answered = true;
                 extern.richtigeAntworten++;
             }
             else
             {
                 antwort.counter = Color.red;
                 answered = true;
                 extern.richtigeAntworten = extern.richtigeAntworten;
             }
         } */
    }

    IEnumerator ModeSelect()
    {
        isRunning = true;
        messagePreFilter();
        yield
        return new WaitForSeconds(1);
        isRunning = false;
    }

}