var Cylon = require('cylon');

Cylon.robot({
  connections: {
    mqtt: { adaptor: 'mqtt', host: 'mqtt://localhost:1883' },
    leapmotion: { adaptor: 'leapmotion' }
  },

  devices: {
    server: { driver: 'mqtt', topic: 'leaptest', adaptor: 'mqtt' },
    //leapmotion: { driver: 'leapmotion' }
  },

  work: function(my) {
      my.leapmotion.on('gesture', function(gesture) {
           
          // Steuerung für Gesten
          var gestureString =  JSON.stringify(gesture);
          //my.server.publish(gestureString, 2); 
          //console.log(gesture);
          var gesten = JSON.parse(gestureString);      
         
          
          if(gesten.type == "swipe" && gesten.state == "stop")
          {                
                // console.log("swipe");
                my.server.publish("swipe " + Date.now() / 1000000);
              
          }
           if(gesten.type == "keyTap")
          {
                // console.log("keyTap " + Date.now() / 1000000);
                my.server.publish("keyTap" + "," + Date.now() / 1000000);
          }
           if(gesten.type == "screenTap")
          {
                // console.log("tap");
                my.server.publish("screenTap" + "," + Date.now() / 1000000);
          }
           if(gesten.type == "circle" && gesten.radius >= 20 && gesten.state == "stop")
          {
                // console.log("kreis");
                my.server.publish("circle" + "," + Date.now() / 1000000);
          }   
          
          // Steuerung für Positional Tracking der Hand
          /*
          var palmX = hand.palmX;
          var palmY = hand.palmY;
          var palmZ = hand.palmZ;
          
          console.log(palmX + " " + palmY + " "  + palmZ);  
          my.server.publish(palmX + "," + palmY + ","  + palmZ);
          */
                    
    }); 
    
      
  }
}).start();