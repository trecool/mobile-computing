Unity3D VR e-Learning mit Leap Motion
============

Benötigte Tools (Windows)
--------------
* Mosquitto
* NodeJS
* Unity 3D (aktuelleste Version)
* Leap Motion Orion

CylonJS
--------------
Für CylonJS werden die Module für die Leap Motion sowie MQTT benötigt
* npm install cylon cylon-leapmotion für die Leap
* npm install cylon cylon-mqtt für MQTT

Installation (Windows, Mac vermutlich ähnlich)
--------------
Nachdem alle benötigten Tools installiert worden sind, der Mosquitto Broker starten und im Anschluss die Datei cylon.js mit dem Befehl node cylon.js ausführen.

Unter "Datei --> Projekt öffnen" in Unity den Projektordner auswählen und alle Assets laden lassen. 

Mit dem Play-Button lässt sich die Szene starten.

Ordnerstruktur Unity
--------------
**Cardboard**
Enthält alle benötigten Dateien für die Darstellung in einem Cardboard-System.

**JSON**
Hier liegt die (für diese Anwendung stark modifizierte) JSON-Datei.

**Materials**
Materials der 3D-Objekte der Szene.

**Model**
Externe 3D-Objekte.

**MQTT**
Inhalte des MQTT-Plugins für Unity.

**Plugins**
Teil von Google Cardboard.

**Prefabs**
Enthält ein "UI"-Prefabs. Prefabs sind vordefinierte Objekte in Unity, die einfach in Szenen  platziert werden können. Dieses Prefabs enthält alles was zur Darstellung und Beantwortung von Fragen in Unity benötigt wird.

**Resources**
Einzelne Frames des Videos als JPEG.

**Scenes**
Alle Szenen. 
Loading - Ladebildschirm
Main - Die Hauptapplikation für Mobile Computing
UI_Fragen - Szene, in der das UI-Prefabs gebaut wurde
Zusatzmodul - Tutorial für das Modul IfM

**Scripts**
Alle benötigten Scripts.
GameController - Steuerung des gesamten Ablaufs der Anwendung in Unity.
JSONSerializer - Einlesen und Verarbeitung der JSON-Dateien.
LevelLoader - Laden von Szenen.
Rotation - Drehung der Kreise und des Zylinders.
TexturePlayer - Script um JPEGs als "Film" mobil abspielen zu können.
TutorialController - Steuerung des Zusatzmoduls für IfM.
UIController - Steuerung des Fragen-Prefabs.

*Ungenutzt*
GestenControls -  Veränderung der Farbe von Objekten basierend auf Gesten.
getValues - Script, welches ein 3D-Tracking der Hand im Raum auf Basis der Daten der Leap erlaubt.

**Textures**
Alle benötigten Texturen.

**Tutorial**
Assets für das Zusatzmodul für IfM.

Anmerkungen
--------------
Falls ein 3DSMax-Fehler erscheint, einfach das Objekt "Operationstisch" entfernen. 
Sollte der Fehler bestehen bleiben, den Ordner "Model" löschen.

Für den Build für Android / iOS werden entsprechende Plug-Ins benötigt, die man bei der Installation von Unity auswählen kann. Android benötigt noch Android Studio, danach können Projekte als .apk exportiert werden oder direkt mit Entwicklertools auf dem Smartphone ausgeführt werden.

Bekannte Fehler 
--------------
* Gesten werden schlecht erkannt (Problem seitens der Leap Motion)
* Video ohne Ton (aufgrund der Art der Wiedergabe)
* Video muss komplett ausgblendet werden um Text anzeigen zu können

Externe Tools / Plug-Ins / Code
--------------
[Google Cardboard](https://developers.google.com/vr/unity/)
[MQTT for Unity3D](https://github.com/vovacooper/Unity3d_MQTT)
[Mobile Videos in Unity](http://bernieroehl.com/360stereoinunity/)